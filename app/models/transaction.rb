class Transaction < ApplicationRecord

  belongs_to :balance

  def self.buy(balance,transaction_params)
    #puts "Buying gold worth of #{transaction_params.amount} into account #{balance.id}"
    #return false unless self.amount_valid?(amount)
    ActiveRecord::Base.transaction do
      self.take_gold(balance,transaction_params)
      self.give_cash(balance,transaction_params)
      self.create_transaction(balance,transaction_params)
    end
  end
  def self.take_gold(balance,transaction_params)
    puts "Buying gold worth of #{transaction_params.amount}"
    amount = transaction_params.amount / 10
    puts "Take gold worth #{amount}"
    balance.gold_balance = (balance.gold_balance += amount).round(2)
    puts "your gold balance #{balance.gold_balance} in account #{balance.id}"
    balance.save!
  end
  def self.give_cash(balance,transaction_params)
    balance.cash_balance = (balance.cash_balance -= transaction_params.amount).round(2)
    balance.save!
  end

  def self.sell(balance,transaction_params)
    #puts "Selling gold worth of #{transaction_params.amount} from account #{balance.id}"
    ActiveRecord::Base.transaction do
      self.give_gold(balance,transaction_params)
      self.take_cash(balance,transaction_params)
      self.create_transaction(balance,transaction_params)
    end
  end
  def self.give_gold(balance,transaction_params)
    puts "Selling gold worth of #{transaction_params.amount}"
    amount = transaction_params.amount / 10
    puts "Give gold worth #{amount}"
    balance.gold_balance = (balance.gold_balance -= amount).round(2)
    puts "your gold balance #{balance.gold_balance} in account #{balance.id}"
    balance.save!
  end
  def self.take_cash(balance,transaction_params)
    puts"Adjust the adjusted_bal with #{transaction_params.amount} on #{balance.id}"
    balance.cash_balance = (balance.cash_balance += transaction_params.amount).round(2)
    puts"The adjusted_bal is#{balance.cash_balance} accountID: #{balance.id}"
    balance.save!
  end
  def self.topup(balance,transaction_params)#balance,
    puts "Topup your account #{transaction_params.balance_id} with #{transaction_params.amount}"
    ActiveRecord::Base.transaction do
      self.create_transaction(balance,transaction_params)
      self.take_cash(balance,transaction_params)
    end
  end
  def self.create_transaction(balance,transaction_params)
    puts"Create transaction with #{transaction_params.amount} on #{transaction_params.balance_id}"
   #Create the transaction object
    Transaction.create!(transaction_type: transaction_params.transaction_type,
    asset: transaction_params.asset, amount: transaction_params.amount,
    balance_id: transaction_params.balance_id)
  end

end
