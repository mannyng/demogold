require 'chasing_news'
module Api
class NewsController < ApplicationController
    #before_action :authenticate_request!, only: [:show]

  def index
    @allnews = News.all

    render json: @allnews
  end
  def check_news
    payload = 'https://www.marketwatch.com/'
    doc = Nokogiri::HTML(open(payload))

    puts "### Search for nodes by css"
    @Note = []
    doc.css('div[data-track-code="MW_Homepage_Automated_The Moneyist|MW_Homepage_Automated_The Moneyist_Ticker"] ul li').each do |link|
      if link.at_css('h3')
      href = link.at_css('h3 a')["href"]
      h2 = link.at_css('h3').text
      @Note << {:href => href,:h2 => h2}
      puts h2
     else
       puts "## No data here"
     end
    end
     if @Note
       render json: { newsUrl: @Note}
     else
       render json: {error: @Note.errors}, status: :unauthorized
     end
  end
  def check_baltic_news
    payload = 'https://www.baltictimes.com/'
    #@Note = ChasingNews.scrapeBalticNews(payload)
    doc = Nokogiri::HTML(open(payload))
    @Note = []
    doc.css('div[class="row blog blog-medium margin-bottom-20"]').each do |link|
      if link.at_css('a')
      href = link.at_css('a')["href"]
      h2 = link.at_css('p').text
      @Note << {:fat_href => href,:fat_h2 => h2}
      News.create( :fat_href => href,:fat_h2 => h2,:news_url => payload)
      puts h2
     else
       puts "## No data here"
     end
    end
    doc.css('div[class="col-xs-12"]').each do |link|
      if link.at_css('h5 a')
        slim_href = link.at_css('h5 a')["href"]
        slim_h5 = link.at_css('h5 a').text
        @Note << {:slim_href => slim_href, :slim_h5 => slim_h5}
        News.create(:slim_href => slim_href,:news_url => payload)
        else
        puts "## No h5 data here"
      end
    end
    render json: @Note
  end

   def new
   end
   def create
   end
   def show
   end
 end
end
