module Api
class BalancesController < ApplicationController
  #before_action :authenticate_request!
  before_action :set_balance, only: [:edit, :update, :destroy]
  #before_action :user_owns_account?

  def index
   #@customer = Customer.find_by_user_id(current_user.id)
   @balances = Balance.all

   render json: @balances
  end

  # GET /balances/1
  # GET /balances/1.json
  def show
    @user_balance = Balance.find_by_customer_id(params[:id])
    render json: @user_balance
  end

  def new
  end

  def edit
  end

  def create
    @balance = Balance.new(balance_params)
    #@customer = Customer.find(@balance.customer_id)
    @balance.cash_balance = 0.00
    @balance.gold_balance = 0.00

      if @balance.save!
          render json: @balance, status: :created
      else
        render json: { errors: @balance.errors }, status: :unprocessable_entity
      end
  end

  def update
    if @balance.update(account_params)
        render json: @balance, status: :ok
      else
        render json:  { errors: @balance.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @balance.destroy
      #render json: { head :no_content }
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_balance
      @balance = Balance.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def balance_params
      params.require(:balance).permit(:customer_id)
    end

    # User may only access their own account(s), unless they are admin
    def user_owns_balance?
      if @balance
        @balance.customer.user_id == current_user.id || current_user.role == 'admin'
      end
    end
 end
end
