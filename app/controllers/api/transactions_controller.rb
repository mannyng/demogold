module Api
class TransactionsController < ApplicationController
  #before_action :authenticate_request!
  before_action :set_acct_transaction, only: [:show, :edit, :update, :destroy]

  # GET /acct_transactions
  # GET /acct_transactions.json
  def index
    @transactions = Transaction.all

    render json: @transactions
  end
  # GET /acct_transactions/1/edit
  def edit
  end
  def show
  end
 #POST   /transactions/top_up
  def top_up
    @transaction_params = Transaction.new(transaction_params)
    @balance = Balance.find(@transaction_params.balance_id )
    @transaction = Transaction.topup(@balance,@transaction_params) #@balance,
     if @transaction
     render json: @transaction
      else
       render json: {errors: @transaction.errors}, status: :unprocessable_entity
      end
   end
   def buy
     @transaction_params = Transaction.new(transaction_params)
     @balance = Balance.find(@transaction_params.balance_id )
     @transaction = Transaction.buy(@balance,@transaction_params) #@balance,
      if @transaction
      render json: @transaction
       else
        render json: {errors: @transaction.errors}, status: :unprocessable_entity
       end
    end
    def sell
      @transaction_params = Transaction.new(transaction_params)
      @balance = Balance.find(@transaction_params.balance_id )
      @transaction = Transaction.sell(@balance,@transaction_params) #@balance,
       if @transaction
       render json: @transaction
        else
         render json: {errors: @transaction.errors}, status: :unprocessable_entity
        end
     end

  # POST /acct_transactions
  # POST /acct_transactions.json
  def create
    @transaction = Transaction.new(transaction_params)
    if @transaction_params.transaction_type == 'topup'

    @transaction.save!

      if @transaction.valid?
        render json: @transaction
      else
        render json: {errors: @transaction.errors}, status: :unprocessable_entity
      end
   end
end
  # PATCH/PUT /acct_transactions/1
  # PATCH/PUT /acct_transactions/1.json
  def update
    if @transaction.update(transaction_params)
       render json: @transaction, status: :ok
    else
       render json: {errors: @transaction.errors}, status: :unprocessable_entity
    end
  end

  # DELETE /acct_transactions/1
  # DELETE /acct_transactions/1.json
  def destroy
    @transaction.destroy
      #render json: { head :no_content }
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_params
      params.require(:transaction).permit(:balance_id, :transaction_type, :asset, :amount)
    end
 end
end
