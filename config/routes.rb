Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
   constraints constraints: { subdomain: 'api', }, defaults: { format: 'json' } do
   resources :users, only: [:create,:new, :show, :index] do
    collection do
     post 'confirm'
     post 'login'
    end
   end
   resources :news do
    collection do
     get 'check_news'
     get 'check_baltic_news'
    end
  end
  resources :customers
  resources :balances
  resources :transactions do
   collection do
    post 'top_up'
    post 'buy'
    post 'sell'
   end
  end
  end
  end
end
