# README

I used rails 'rails', '~> 5.2.0' and ruby '2.3.7' with postgresql gem to setup the demo. I used Rails Unit test.

1. http://demogoldback-rorturtola.c9users.io/api/transactions - This provides a listing of all the transactions in the DB
2. http://demogoldback-rorturtola.c9users.io/api/transactions/top_up - This allows a top up of cash ​
3. http://demogoldback-rorturtola.c9users.io/api/transactions/buy - This allows the user to buy gold
4. http://demogoldback-rorturtola.c9users.io/api/transactions/sell - This allows the user to sell gold
5. http://demogoldback-rorturtola.c9users.io/api/balances - This returns the users current balance of gold and cash

I have liberalized the CORS so any host can send request to the backend api and recieve response from it as well. And I have commented out the before_action filter so even if you are not logged in you scan still use the api.

If you want to see the implementation in action please check it out here.
http://demogold-rorturtola.c9users.io/
username: belshik@aol.com
password: milky

The frontend is running on ReactJs/Redux.
In order to see how the buy or sell or topup work in realtime you need to login.
