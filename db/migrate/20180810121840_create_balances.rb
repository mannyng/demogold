class CreateBalances < ActiveRecord::Migration[5.2]
  def up
    create_table :balances do |t|
      t.references :customer
      t.decimal :cash_balance,    precision: 10, scale: 2, null: false
      t.decimal :gold_balance,    precision: 10, scale: 2, null: false
      t.timestamps
    end
  end
  def down
    drop_table :balances
  end
end
