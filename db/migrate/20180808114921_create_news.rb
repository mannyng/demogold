class CreateNews < ActiveRecord::Migration[5.2]
  def up
    create_table :news do |t|
      t.string :news_url
      t.text :fat_h2
      t.text :fat_href
      t.text :slim_h2
      t.text :slim_href

      t.timestamps
    end
    add_index "news", ["news_url"],name: "NEWS_URL", using: :btree
  end
  def down
    drop_table :news
  end
end
