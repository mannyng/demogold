class CreateTransactions < ActiveRecord::Migration[5.2]
  def up
    enable_extension 'uuid-ossp'
    create_table :transactions do |t|
      t.references :balance
      t.uuid :txref, null: false, default: 'uuid_generate_v4()'
      t.string :transaction_type
      t.string :asset
      t.decimal :amount
      t.timestamps
    end
    add_index :transactions, :txref, unique: true
  end
  def down
    drop_table :transactions
  end
end
