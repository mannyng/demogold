class CreateCustomers < ActiveRecord::Migration[5.2]
  def up
    create_table :customers do |t|
      t.string  "phone",    limit: 20
      t.string  "firstname", limit: 40
      t.string  "lastname",  limit: 40
      t.references    "user",  null: false

      t.timestamps
    end
    add_index "customers", ["lastname", "firstname"], name: "NAME_LAST_FIRST", using: :btree
  end
  def down
    drop_table :customers
  end
end
