require 'nokogiri'
require 'open-uri'

class ChasingNews

def self.scrapeMarketNews()
# Fetch and parse HTML document
  payload = 'https://www.marketwatch.com/'
  doc = Nokogiri::HTML(open(payload))
  @Note = []
  doc.css('div[data-track-code="MW_Homepage_Automated_The Moneyist|MW_Homepage_Automated_The Moneyist_Ticker"] ul li').each do |link|
    if link.at_css('h3')
    href = link.at_css('h3 a')["href"]
    h2 = link.at_css('h3').text

    @News.create( :fat_href => href,:fat_h2 => h2,:news_url => payload)
    puts h2
   else
     puts "## No data here"
   end
  end
end
def self.scrapeBalticNews(payload)
# Fetch and parse HTML document
  #payload = 'https://www.marketwatch.com/'
  doc = Nokogiri::HTML(open(payload))
  @Note = []
  doc.css('div[class="row blog blog-medium margin-bottom-20"]').each do |link|
    if link.at_css('a')
    href = link.at_css('a')["href"]
    h2 = link.at_css('p').text
    @Note << {:fat_href => href,:fat_h2 => h2}
    #News.create( :fat_href => href,:fat_h2 => h2,:news_url => payload)
    puts h2
   else
     puts "## No data here"
   end
  end
  doc.css('div[class="col-xs-12"]').each do |link|
    if link.at_css('h5 a')
      slim_href = link.at_css('h5 a')["href"]
      slim_h5 = link.at_css('h5 a').text
      @Note << {:slim_href => slim_href, :slim_h5 => slim_h5}
      #News.create(:slim_href => slim_href,:news_url => payload)
      puts slim_h5
    else
      puts "## No h5 data here"
    end
  end
  @Note.each do |link|
    News.create(:fat_href => link.fat_href, :fat_h2 => link.fat_h2,
       :slim_href => slim_href, :slim_h5 => slim_h5,:news_url => payload)
  end
  return
end
def self.scraper
Nokogiri::XML::Node.send(:define_method, 'xpath_regex') { |*args|
  xpath = args[0]
  rgxp = /\/([a-z]+)\[@([a-z\-]+)~=\/(.*?)\/\]/
  xpath.gsub!(rgxp) { |s| m = s.match(rgxp); "/#{m[1]}[regex(.,'#{m[2]}','#{m[3]}')]" }
  self.xpath(xpath, Class.new {
    def regex node_set, attr, regex
      node_set.find_all { |node| node[attr] =~ /#{regex}/ }
    end
  }.new)
}
end
  puts "### Search for nodes by xpath"
  #doc.xpath('//nav//ul//li/a', '//article//h2').each do |link|
  #  puts link.content
  #end

  #puts "### Or mix and match."
  #doc.search('nav ul.menu li a', '//article//h1', '//article//h2', '//article//h3').each do |link|
  #  puts link.text
  #end
  #doc.to_html
  def self.seachregex
    Nokogiri::XML::Node.send(:define_method, 'xpath_regex') { |*args|
    xpath = args[0]
    rgxp = /\/([a-z]+)\[@([a-z\-]+)~=\/(.*?)\/\]/
    xpath.gsub!(rgxp) { |s| m = s.match(rgxp); "/#{m[1]}[regex(.,'#{m[2]}','#{m[3]}')]" }
    self.xpath(xpath, Class.new {
    def regex node_set, attr, regex
      node_set.find_all { |node| node[attr] =~ /#{regex}/ }
    end
   }.new)
  }
  end

end
