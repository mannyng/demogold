
require 'test_helper'

class TransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
      @transaction = transactions(:new_transaction)
      @balance = balances(:balance)
    end
    teardown do
     # when controller is using cache it may be a good idea to reset it afterwards
     Rails.cache.clear
    end

    test "should get sell response" do
      response = Transaction.sell(@balance,@transaction)

      assert response
    end
    test "should get buy response" do
      response = Transaction.buy(@balance,@transaction)

      assert response
    end
    test "should get top up response" do
      response = Transaction.topup(@balance,@transaction)

      assert response
    end
    
end
